import torch
from metrics import accuracy


def train(model, train_dataloader, optimizer, criterion, log_writer=None):
    model.train()

    epoch_loss = []
    epoch_acc = []

    total_samples = len(train_dataloader.dataset.indices)

    for batch_indx, (video_clips, labels) in enumerate(train_dataloader):
        preds = []  # video_clips.permute(0, 2, 1, 3, 4)

        optimizer.zero_grad()

        for video_clip, label in zip(video_clips, labels):
            optimizer.zero_grad()
            #pred = model(video_clips.permute(0, 2, 1, 3, 4))
            pred = model(video_clip)
            preds.append(torch.argmax(pred, 0).item())
            loss = criterion(pred.unsqueeze(0), label.unsqueeze(0))
            loss.backward()

            #torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=5.0)

            optimizer.step()
            epoch_loss.append(loss.item())

        epoch_acc.append(accuracy(torch.tensor(preds), labels))

        if batch_indx != 0 and batch_indx % 2 == 0:
            print('[{}/{}] loss: {:.3f}, accuracy: {:.3f}'.format(batch_indx * train_dataloader.batch_size, total_samples,
                                                                  sum(epoch_loss) / len(epoch_loss),
                                                                  sum(epoch_acc) / len(epoch_acc)
                                                                  ))
        if log_writer:
            log_writer.add_scalar('fc_image_map.weight', model.fc_image_map.weight.mean().item(), batch_indx)
            log_writer.add_scalar('fc_image_map.weight_grad', model.fc_image_map.weight.grad.mean().item(), batch_indx)
            log_writer.add_scalar('fc_end.weight', model.fc_end.weight.mean().item(), batch_indx)
            log_writer.add_scalar('fc_end.weight_grad', model.fc_end.weight.grad.mean().item(), batch_indx)
            # RNN
            # log_writer.add_scalar('model.rnn.weight_ih_l4', model.rnn.weight_ih_l4.mean().item(), batch_indx)
            # log_writer.add_scalar('model.rnn.weight_ih_l4_grad', model.rnn.weight_ih_l4.grad.mean().item(), batch_indx)
            log_writer.add_scalar('model.rnn.weight_ih_l1', model.rnn.weight_ih_l1.mean().item(), batch_indx)
            log_writer.add_scalar('model.rnn.weight_ih_l1_grad', model.rnn.weight_ih_l1.grad.mean().item(), batch_indx)
            # Score
            log_writer.add_scalar('Loss-batch', sum(epoch_loss) / len(epoch_loss), batch_indx)
            log_writer.add_scalar('Accuracy-batch', sum(epoch_acc) / len(epoch_acc), batch_indx)

    else:
        print('[{}/{}] loss: {:.3f}, accuracy: {:.3f}'.format(batch_indx * train_dataloader.batch_size, total_samples,
                                                              sum(epoch_loss) / len(epoch_loss),
                                                              sum(epoch_acc) / len(epoch_acc)
                                                              ))
    return sum(epoch_loss) / len(epoch_loss), sum(epoch_acc) / len(epoch_acc)


def test(model, test_dataloader, criterion):
    model.eval()

    epoch_loss = []
    epoch_acc = []

    with torch.no_grad():
        for video_clips, labels in test_dataloader:
            preds = []
            for video_clip, label in zip(video_clips, labels):
                pred = model(video_clip)
                preds.append(torch.argmax(pred, 0).item())
                loss = criterion(pred.unsqueeze(0), label.unsqueeze(0))
                epoch_loss.append(loss.item())

            epoch_acc.append(accuracy(torch.tensor(preds), labels))

    return sum(epoch_loss) / len(epoch_loss), sum(epoch_acc) / len(epoch_acc)

