import os
from os.path import abspath, dirname, join
import torchvision
from Data.UCF101.dataset import save_dataset, load_dataset, transform_ucf101_for_inception3


def get_train_test_datasets(size='small', length_clip_frames=120):
    BASE_ROOT = abspath(dirname(__file__))

    if size == 'small':
        ROOT = abspath(join(BASE_ROOT, './Data/UCF101/data_small/video_clips'))
        ANNOTATION = abspath(join(BASE_ROOT, './Data/UCF101/data_small/train_test_list'))
    if size == 'big':
        ROOT = abspath(join(BASE_ROOT, './Data/UCF101/data'))
        ANNOTATION = abspath(join(BASE_ROOT, './Data/UCF101/train_test_list'))

    if not os.path.exists('train_dataset_UCF101.dataset'):
        train_action_recognition_dataset = torchvision.datasets.UCF101(root=ROOT, annotation_path=ANNOTATION,
                                                                       frames_per_clip=length_clip_frames,
                                                                       step_between_clips=length_clip_frames,
                                                                       transform=transform_ucf101_for_inception3,
                                                                       train=True)

        save_dataset('train_dataset_UCF101.dataset', train_action_recognition_dataset)

    train_action_recognition_dataset = load_dataset('train_dataset_UCF101.dataset')

    if not os.path.exists('test_dataset_UCF101.dataset'):
        test_action_recognition_dataset = torchvision.datasets.UCF101(root=ROOT, annotation_path=ANNOTATION,
                                                                      frames_per_clip=length_clip_frames,
                                                                      step_between_clips=length_clip_frames,
                                                                      transform=transform_ucf101_for_inception3,
                                                                      train=False)

        save_dataset('test_dataset_UCF101.dataset', test_action_recognition_dataset)

    test_action_recognition_dataset = load_dataset('test_dataset_UCF101.dataset')

    return train_action_recognition_dataset, test_action_recognition_dataset


if __name__ == '__main__':


    print('Done')

