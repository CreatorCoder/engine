import torch
from torch import optim, nn
from torch.utils.data import DataLoader
from torchvision import models
from data_train_test import get_train_test_datasets
from start import custom_collate

EPOCHS = 10


if __name__ == '__main__':

    train_dataset, test_dataset = get_train_test_datasets(size='small', length_clip_frames=120)  # 'big'

    print('Train dataset size: {}'.format(len(train_dataset.indices)))
    print('Test dataset size: {}'.format(len(test_dataset.indices)))
    print('Count classes: {}'.format(len(train_dataset.classes)))

    train_dataloader = DataLoader(dataset=train_dataset, batch_size=16, num_workers=6, shuffle=True, drop_last=True,
                                  collate_fn=custom_collate)
    test_dataloader = DataLoader(dataset=test_dataset, batch_size=32, num_workers=6, shuffle=True, drop_last=True,
                                 collate_fn=custom_collate)

    model = models.alexnet(pretrained=False, progress=True, num_classes=4)

    optimizer = optim.Adam(lr=1e-3, params=model.parameters())
    criterion = nn.CrossEntropyLoss()

    for epoch in range(1, EPOCHS):

        # train
        model.train()
        epoch_loss = []
        for batch_indx, (video_clips, labels) in enumerate(train_dataloader):

            for video_clip, label in zip(video_clips, labels):
                optimizer.zero_grad()
                preds = model(video_clip)
                targets = torch.ones((120,), dtype=torch.int64)*label
                loss = criterion(preds, targets)
                loss.backward()
                optimizer.step()
                epoch_loss.append(loss.item())

        train_loss = sum(epoch_loss) / len(epoch_loss)

        print(f'Epoch: {epoch}, Train Loss: {train_loss:.3f}')

        #test
        model.eval()
        epoch_loss = []
        with torch.no_grad():
            for video_clips, labels in test_dataloader:
                for video_clip, label in zip(video_clips, labels):
                    preds = model(video_clip)
                    targets = torch.ones((120,), dtype=torch.int64) * label
                    loss = criterion(preds, targets)
                    epoch_loss.append(loss.item())

        test_loss = sum(epoch_loss) / len(epoch_loss)

        print(f'Epoch: {epoch}, Test Loss: {test_loss:.3f}')

        # save the model
        torch.save(model.state_dict(), 'alexnet_weights.model')
