import torch
from torch import nn
from torchvision import models

alexnet = models.alexnet(pretrained=False, num_classes=4)
alexnet_weights = torch.load('alexnet_weights.model')
alexnet.load_state_dict(alexnet_weights)


class ActionDetector(nn.Module):

    def __init__(self, n_classes, n_layers_lstm=5, len_frames=24, hidden_size=384):
        super().__init__()

        #self.backbone = torch.hub.load('pytorch/vision:v0.6.0', 'inception_v3', pretrained=True)
        self.backbone_features = alexnet.features
        self.backbone_avgpool = alexnet.avgpool

        for param in self.backbone_features.parameters():
             param.requires_grad = False

        #self.backbone.fc_image_map = nn.Linear(in_features=2048, out_features=384)

        self.hidden_size = hidden_size

        self.fc_image_map = nn.Linear(in_features=9216, out_features=self.hidden_size)
        self.act_image_map = nn.LeakyReLU()
        self.drop_image_map = nn.Dropout(p=0.5)
        self.n_layers_lstm = n_layers_lstm
        self.batch_size_lstm = len_frames  # длина видеоролика (количество кадров)
        self.rnn = nn.LSTM(input_size=self.hidden_size, hidden_size=self.hidden_size, num_layers=self.n_layers_lstm)

        self.fc_end = nn.Linear(len_frames*self.hidden_size*self.n_layers_lstm, out_features=n_classes)
        self.acc_end = nn.LeakyReLU()
        self.acc_end_softmax = nn.LogSoftmax(dim=0)
        self.softmax = torch.nn.functional.softmax
        self.sigmoid = torch.nn.functional.sigmoid

    def forward(self, video_clip):
        #CNN
        #image_map, image_output = self.backbone(video_clip)
        image_map = self.backbone_features(video_clip)
        image_map = self.backbone_avgpool(image_map)
        image_map = torch.flatten(image_map, 1)
        image_map = self.drop_image_map(image_map)
        #image_map = self.act_image_map(self.fc_image_map(image_map))

        # FC
        image_map = self.fc_image_map(image_map)
        image_map = self.act_image_map(image_map)
        image_map = torch.unsqueeze(image_map, 0)

        # RNN
        hn = torch.randn(self.n_layers_lstm, self.batch_size_lstm, self.hidden_size)
        cn = torch.randn(self.n_layers_lstm, self.batch_size_lstm, self.hidden_size)
        _, (hn, cn) = self.rnn(image_map, (hn, cn))  # output not used

        # FC
        output = self.fc_end(hn.view(-1))
        output = self.acc_end(output)
        #output = self.acc_end(self.fc_end(hn.view(-1)))
        #output = self.acc_end_softmax(output)
        #output = self.softmax(output)
        #output = self.sigmoid(output)
        return output


if __name__ == '__main__':

    action_detector = ActionDetector(n_classes=101, n_layers_lstm=2)
