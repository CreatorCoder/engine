from os.path import abspath
import torch
import torchvision
from Data.UCF101.dataset import save_dataset, transform_ucf101_for_inception3, load_dataset
import matplotlib.pyplot as plt


if __name__ == '__main__':

    # -----------------  DATA ------------------
    # ROOT = abspath('../../../Data/UCF101/data')
    # ANNOTATION = abspath('../../../Data/UCF101/train_test_list')
    #
    # action_recognition_dataset = torchvision.datasets.UCF101(root=ROOT, annotation_path=ANNOTATION,
    #                                                          frames_per_clip=15, step_between_clips=15,
    #                                                          transform=transform_ucf101_for_inception3)
    #
    # save_dataset('dataset_UCF101.dataset', action_recognition_dataset)
    action_recognition_dataset = load_dataset('dataset_UCF101.dataset')

    # --------------- MODEL -------------------
    model = torch.hub.load('pytorch/vision:v0.6.0', 'inception_v3', pretrained=True)

    fig = plt.figure()

    for i in range(len(action_recognition_dataset)):
        frames, _, label = action_recognition_dataset[i]
        count_frames = 0
        for frame in frames:
            plt.imshow(frame.permute(1, 2, 0) .numpy())
            plt.show()
            count_frames += 1
            print('Shape: {}'.format(frame.shape))
        print('{}:{}'.format(count_frames, label))
        break

    with torch.no_grad():

        output = model(frames)
        # Tensor of shape 1000, with confidence scores over Imagenet's 1000 classes
        print(output[0].shape)

        # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
        print(torch.argmax(torch.nn.functional.softmax(output[0][0], dim=0)))

