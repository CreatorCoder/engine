import glob
import os
from os.path import abspath

import torch
from torch import nn, optim
from torch.utils.data.dataloader import DataLoader
from data_train_test import get_train_test_datasets
from model_p3d import P3D199
from train_test import train, test
from model_cnn_rnn import ActionDetector
from torch.utils.tensorboard import SummaryWriter

import warnings
warnings.filterwarnings("ignore")

EPOCHS = 50




def custom_collate(batch):
    """
    Эта функция правит ошибку разныого размера тензоров у аудио дорожки
    :param batch:
    :return:
    """
    filtered_batch = []
    for video, _, label in batch:
        filtered_batch.append((video, label))
    return torch.utils.data.dataloader.default_collate(filtered_batch)


def clear_folder(path):
    files = glob.glob(path + '/*')
    for f in files:
        os.remove(f)


# Logging
writer_tensorboard = SummaryWriter()


if __name__ == '__main__':

    # Чистим логи
    #clear_folder(abspath('./runs'))

    train_dataset, test_dataset = get_train_test_datasets(size='small', length_clip_frames=24)  # 'big'

    print('Train dataset size: {}'.format(len(train_dataset.indices)))
    print('Test dataset size: {}'.format(len(test_dataset.indices)))
    print('Count classes: {}'.format(len(train_dataset.classes)))

    train_dataloader = DataLoader(dataset=train_dataset, batch_size=4, num_workers=2, shuffle=True, drop_last=True,
                                  collate_fn=custom_collate)
    test_dataloader = DataLoader(dataset=test_dataset, batch_size=4, num_workers=2, shuffle=True, drop_last=True,
                                 collate_fn=custom_collate)

    model = ActionDetector(n_classes=len(train_dataset.classes), n_layers_lstm=2, len_frames=24, hidden_size=900)

    # model = P3D199(pretrained=True, modality='RGB', num_classes=600)
    # model.fc_image_map = nn.Linear(in_features=2048, out_features=4)

    params = len([param for param in model.parameters() if param.requires_grad==True])
    print('Learning params: {}'.format(params))

    optimizer = optim.Adam(lr=1e-6, params=model.parameters())
    #optimizer = optim.SGD(lr=1e-4, params=model.parameters())
    criterion = nn.CrossEntropyLoss()

    # Запишем структуру нейронной сети
    writer_tensorboard.add_graph(model, train_dataset[0][0])

    for epoch in range(1, EPOCHS):

        # train the model
        train_loss, train_acc = train(model, train_dataloader, optimizer, criterion, writer_tensorboard)

        writer_tensorboard.add_scalar("Loss/train", train_loss, epoch)
        writer_tensorboard.add_scalar("Accuracy/train", train_acc, epoch)

        # test the model
        test_loss, test_acc = test(model, test_dataloader, criterion)

        writer_tensorboard.add_scalar("Loss/test", test_loss, epoch)
        writer_tensorboard.add_scalar("Accuracy/test", test_acc, epoch)

        # save the model
        torch.save(model.state_dict(), 'cnn_rnn_weights.model')

        print(f'Epoch: {epoch}, Train Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
        print(f'Epoch: {epoch}, Test. Loss: {test_loss:.3f} |  Val. Acc: {test_acc*100:.2f}%')


    writer_tensorboard.close()
    print('Done')
