import torch


def accuracy(predicts, targets):
    correct = (predicts == targets).float()
    acc = correct.sum() / len(correct)
    #return acc.item()
    return acc
